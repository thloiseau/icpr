#!/usr/bin/env python
import roslib; roslib.load_manifest('laser_assembler')
import rospy; from laser_assembler.srv import *
from sensor_msgs.msg import PointCloud
 
rospy.init_node("periodic_snapshotter")
pub = rospy.Publisher('rviz/cloud', PointCloud, queue_size=10)
rate = rospy.Rate(0.2)
rospy.wait_for_service("assemble_scans")
assemble_scans = rospy.ServiceProxy('assemble_scans', AssembleScans)
while not rospy.is_shutdown():
    try:
        resp = assemble_scans(rospy.Time(0,0), rospy.get_rostime())
        msg = resp.cloud
        pub.publish(msg)
        rate.sleep()
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
